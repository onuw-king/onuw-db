CREATE USER 'username'@'host' IDENTIFIED BY 'password';

GRANT INSERT, UPDATE, SELECT, DELETE ON onuw_games.* TO 'username'@'host';

FLUSH PRIVILEGES;