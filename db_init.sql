delimiter //

set @@global.time_zone = "+00:00";

create database onuw_games;

create table if not exists onuw_games.admin_roles (
    when_record_created datetime not null,
    server_id char(18) not null,
    role_id char(18) unique not null,
    is_active tinyint not null default 1,
    index (
        role_id,
        is_active
    ),
    primary key (role_id)
);

create table if not exists onuw_games.game_metadata (
    when_record_created datetime not null,
    server_id char(18) not null,
    game_master_id char(18) not null,
    channel_id char(18) not null,
    game_config_name char(32),
    game_master_name char(32) not null,
    is_active tinyint not null default 1,
    is_in_session tinyint not null default 0,
    index (
        server_id
    ),
    index (
        game_master_id
    ),
    index (
        channel_id
    ),
    index (
        server_id,
        is_active
    ),
    index (
        game_master_id,
        is_active
    ),
    index (
        channel_id,
        is_active
    ),
    index (
        server_id,
        is_in_session
    ),
    index (
        game_master_id,
        is_in_session
    ),
    index (
        channel_id,
        is_in_session
    ),
    index (
        server_id,
        is_active,
        is_in_session
    ),
    index (
        game_master_id,
        is_active,
        is_in_session
    ),
    index (
        channel_id,
        is_active,
        is_in_session
    ),
    index (
        when_record_created,
        server_id
    ),
    index (
        when_record_created,
        game_master_id
    ),
    index (
        when_record_created,
        channel_id
    ),
    index (
        when_record_created,
        server_id,
        is_active
    ),
    index (
        when_record_created,
        game_master_id,
        is_active
    ),
    index (
        when_record_created,
        channel_id,
        is_active
    ),
    index (
        when_record_created,
        server_id,
        is_in_session
    ),
    index (
        when_record_created,
        game_master_id,
        is_in_session
    ),
    index (
        when_record_created,
        channel_id,
        is_in_session
    ),
    index (
        when_record_created,
        server_id,
        is_active,
        is_in_session
    ),
    index (
        when_record_created,
        game_master_id,
        is_active,
        is_in_session
    ),
    index (
        when_record_created,
        channel_id,
        is_active,
        is_in_session
    ),
    index (
        server_id,
        game_master_id,
        channel_id
    ),
    index (
        server_id,
        game_master_id,
        channel_id,
        is_active
    ),
    index (
        server_id,
        game_master_id,
        channel_id,
        is_in_session
    ),
    index (
        server_id,
        game_master_id,
        channel_id,
        is_active,
        is_in_session
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        channel_id,
        is_active
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        channel_id,
        is_in_session
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        channel_id,
        is_active,
        is_in_session
    ),
    index (
        server_id,
        game_master_id,
        channel_id,
        game_config_name
    ),
    index (
        server_id,
        game_master_id,
        channel_id,
        game_config_name,
        is_active
    ),
    index (
        server_id,
        game_master_id,
        channel_id,
        game_config_name,
        is_in_session
    ),
    index (
        server_id,
        game_master_id,
        channel_id,
        game_config_name,
        is_active,
        is_in_session
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        channel_id,
        game_config_name
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        channel_id,
        game_config_name,
        is_active
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        channel_id,
        game_config_name,
        is_in_session
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        channel_id,
        game_config_name,
        is_active,
        is_in_session
    ),
    index (
        server_id,
        game_master_id,
        game_config_name
    ),
    index (
        server_id,
        game_master_id,
        game_config_name,
        is_active
    ),
    index (
        server_id,
        game_master_id,
        game_config_name,
        is_in_session
    ),
    index (
        server_id,
        game_master_id,
        game_config_name,
        is_active,
        is_in_session
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        game_config_name
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        game_config_name,
        is_active
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        game_config_name,
        is_in_session
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        game_config_name,
        is_active,
        is_in_session
    ),
    index (
        server_id,
        game_master_id
    ),
    index (
        server_id,
        game_master_id,
        is_active
    ),
    index (
        server_id,
        game_master_id,
        is_in_session
    ),
    index (
        server_id,
        game_master_id,
        is_active,
        is_in_session
    ),
    index (
        when_record_created,
        server_id,
        game_master_id
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        is_active
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        is_in_session
    ),
    index (
        when_record_created,
        server_id,
        game_master_id,
        is_active,
        is_in_session
    ),
    primary key (
        when_record_created,
        server_id,
        game_master_id,
        channel_id
    )
) engine=innodb;

create table if not exists onuw_games.players (
    when_game_created datetime not null,
    game_master_id char(18) not null,
    player_id char(18) not null,
    player_name char(32) not null,
    is_in_game tinyint not null default 1,
    index (
        game_master_id
    ),
    index (
        game_master_id,
        is_in_game
    ),
    index (
        when_game_created,
        game_master_id
    ),
    index (
        when_game_created,
        game_master_id,
        is_in_game
    ),
    index (
        player_id
    ),
    index (
        player_id,
        is_in_game
    ),
    index (
        when_game_created,
        player_id
    ),
    index (
        when_game_created,
        player_id,
        is_in_game
    ),
    index (
        game_master_id,
        player_id
    ),
    index (
        game_master_id,
        player_id,
        is_in_game
    ),
    index (
        when_game_created,
        game_master_id,
        player_id,
        is_in_game
    ),
    foreign key(
        when_game_created,
        game_master_id
    ) 
        references onuw_games.game_metadata(
            when_record_created,
            game_master_id
        )
        on update cascade,
    primary key (
        when_game_created,
        game_master_id,
        player_id
    )
) engine=innodb;

create table if not exists onuw_games.saved_config_metadata (
    when_record_created datetime not null,
    when_config_created datetime not null,
    server_id char(18) not null,
    author_id char(18) not null,
    game_config_name char(32) not null,
    author_name char(32) not null,
    is_from_website tinyint not null,
    is_deleted tinyint not null default 0,
    index (
        server_id,
        author_id,
        game_config_name
    ),
    index (
        server_id,
        author_id,
        game_config_name,
        is_deleted
    ),
    index (
        when_config_created,
        server_id,
        author_id,
        game_config_name
    ),
    index (
        when_config_created,
        server_id,
        author_id,
        game_config_name,
        is_deleted
    ),
    primary key (
        when_record_created,
        when_config_created,
        server_id,
        author_id,
        game_config_name
    )
) engine=innodb;

create table if not exists onuw_games.game_settings (
    when_record_created datetime not null,
    server_id char(18) not null,
    user_id char(18) not null,
    game_config_name char(32),
    player_count tinyint not null default -3,
    is_default_day_duration tinyint not null default 1,
    lone_wolf tinyint not null default 1,
    role_duration smallint not null default 30,
    day_duration tinyint not null default -2,
    vote_duration smallint not null default 30,
    foreign key (
        when_record_created,
        server_id,
        user_id,
        game_config_name
    )
        references onuw_games.saved_config_metadata(
            when_config_created,
            server_id,
            author_id,
            game_config_name
        )
        on update cascade,
    foreign key(
        when_record_created,
        server_id,
        user_id
    ) 
        references onuw_games.game_metadata(
            when_record_created,
            server_id,
            game_master_id
        ),
    primary key (
        when_record_created,
        server_id,
        user_id
    )
) engine=innodb;

create table if not exists onuw_games.card_decks (
    when_config_created datetime not null,
    server_id char(18) not null,
    user_id char(18) not null,
    game_config_name char(32),
    villager tinyint not null default 0,
    werewolf tinyint not null default 0,
    seer tinyint not null default 0,
    robber tinyint not null default 0,
    troublemaker tinyint not null default 0,
    tanner tinyint not null default 0,
    drunk tinyint not null default 0,
    hunter tinyint not null default 0,
    mason tinyint not null default 0,
    insomniac tinyint not null default 0,
    minion tinyint not null default 0,
    doppelganger tinyint not null default 0,
    foreign key(
        when_config_created,
        server_id,
        user_id
    )
        references onuw_games.game_metadata(
            when_record_created,
            server_id,
            game_master_id
        ),
    foreign key(
        when_config_created,
        server_id,
        user_id,
        game_config_name
    )
        references onuw_games.saved_config_metadata(
            when_config_created,
            server_id,
            author_id,
            game_config_name
        )
        on update cascade,
    primary key (
        when_config_created,
        server_id,
        user_id,
        game_config_name
    )
) engine=innodb;

create trigger onuw_games.update_player_count_on_card_update
after update on onuw_games.card_decks
for each row
begin
    declare is_default tinyint default 1;
    declare num_cards tinyint default 0;

    select is_default_day_duration into is_default
    from onuw_games.game_settings
    where
        when_record_created = old.when_config_created
        and server_id = old.server_id
        and user_id = old.user_id;

    select
        new.villager +
        new.werewolf +
        new.seer +
        new.robber +
        new.troublemaker +
        new.tanner +
        new.drunk +
        new.hunter +
        new.mason +
        new.insomniac +
        new.minion +
        new.doppelganger
    into num_cards
    from onuw_games.card_decks
    where
        when_config_created = old.when_config_created
        and server_id = old.server_id
        and user_id = old.user_id;

    if is_default = 1 then
        update onuw_games.game_settings
        set
            player_count = num_cards - 3,
            day_phase_duration = num_cards - 2
        where
            when_record_created = old.when_config_created
            and server_id = old.server_id
            and user_id = old.user_id;
    else
        update onuw_games.game_settings
        set player_count = num_cards - 3
        where
            when_record_created = old.when_config_created
            and server_id = old.server_id
            and user_id = old.user_id;
    end if;
end;

create trigger onuw_games.new_game_creation
after insert on onuw_games.game_metadata
for each row
begin
    insert into onuw_games.game_settings(
        when_record_created,
        server_id,
        user_id
    )
    values (
        new.when_record_created,
        new.server_id,
        new.game_master_id
    );
    insert into onuw_games.card_decks(
        when_config_created,
        server_id,
        user_id
    )
    values (
        new.when_record_created,
        new.server_id,
        new.game_master_id
    );
    insert into onuw_games.players(
        when_game_created,
        game_master_id,
        user_id,
        user_name
    )
    values (
        new.when_record_created,
        new.game_master_id,
        new.game_master_id,
        new.game_master_name
    );
end;

create trigger onuw_games.new_game_config_creation
after insert on onuw_games.game_settings
for each row
begin
    insert into onuw_games.card_settings(
        when_config_created,
        server_id,
        user_id,
        game_config_name
    )
    values (
        new.when_record_created,
        new.server_id,
        new.user_id,
        new.game_config_name
    );
end;
//

delimiter ;